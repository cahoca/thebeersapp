import { Component, ViewEncapsulation, AfterContentInit } from '@angular/core';
import { Router } from '@angular/router';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { ActionSheetController } from '@ionic/angular';
import * as urls from '../../providers/urls.servicios';

import { ConferenceData } from '../../providers/conference-data';
import { RegistroUsuariosService } from '../../providers/registro-usuarios.service';
import { Valores } from '../../providers/valores';

@Component({
  selector: 'page-speaker-list',
  templateUrl: 'speaker-list.html',
  styleUrls: ['./speaker-list.scss'],
})
export class SpeakerListPage implements AfterContentInit {
  speakers: any[] = [];
  completo: any;
  URLavatar; any;
  segment = 'amigos';
  amigos = true;

  constructor(
    public actionSheetCtrl: ActionSheetController,
    public confData: ConferenceData,
    public inAppBrowser: InAppBrowser,
    public router: Router,
    public registro: RegistroUsuariosService,
    public valor: Valores
  ) {}


  ngAfterContentInit() {
    this.recargarAmigos();
  }

  actualizarSegment() {
    this.amigos = !this.amigos;
  }

  recargarAmigos() {

    this.completo = 'false';
    // console.log("los speakers antes: " + this.completo);
    // console.log(this.speakers);
    this.URLavatar = urls.URLimagen;
    this.registro.VerAmigos(this.valor.identificacion);
    this.verificarConexion();
  }

  verificarConexion() {
    setTimeout(() => {
      if (this.registro.Amigos.amigos !== undefined) {
        this.speakers = this.registro.Amigos.amigos;
      }
      console.log(this.speakers.length);
      if (this.speakers.length === 0 && this.speakers === undefined) {
        this.completo = 'zero';
      }
      if (this.registro.CargaAmigosCompleta === true) {
        this.completo = 'true';
      }
      if (this.registro.CargaAmigosCompleta === false) {
        this.completo = 'error';
      }
    }, 1500);
  }

  goToSpeakerTwitter(speaker: any) {
    this.inAppBrowser.create(
      `https://twitter.com/${speaker.twitter}`,
      '_blank'
    );
  }

  async openSpeakerShare(speaker: any) {
    const actionSheet = await this.actionSheetCtrl.create({
      header: 'Share ' + speaker.name,
      buttons: [
        {
          text: 'Copy Link',
          handler: () => {
            console.log(
              'Copy link clicked on https://twitter.com/' + speaker.twitter
            );
            if (
              (window as any)['cordova'] &&
              (window as any)['cordova'].plugins.clipboard
            ) {
              (window as any)['cordova'].plugins.clipboard.copy(
                'https://twitter.com/' + speaker.twitter
              );
            }
          }
        },
        {
          text: 'Share via ...'
        },
        {
          text: 'Cancel',
          role: 'cancel'
        }
      ]
    });

    await actionSheet.present();
  }

  async openContact(speaker: any) {
    const mode = 'ios'; // this.config.get('mode');

    const actionSheet = await this.actionSheetCtrl.create({
      header: 'Contact ' + speaker.name,
      buttons: [
        {
          text: `Email ( ${speaker.email} )`,
          icon: mode !== 'ios' ? 'mail' : null,
          handler: () => {
            window.open('mailto:' + speaker.email);
          }
        },
        {
          text: `Call ( ${speaker.phone} )`,
          icon: mode !== 'ios' ? 'call' : null,
          handler: () => {
            window.open('tel:' + speaker.phone);
          }
        }
      ]
    });

    await actionSheet.present();
  }
}
