import { Component, ViewChild, OnInit, AfterViewInit, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController, IonList, LoadingController, ModalController, ToastController } from '@ionic/angular';

import { ScheduleFilterPage } from '../schedule-filter/schedule-filter';
import { ConferenceData } from '../../providers/conference-data';
import { UserData } from '../../providers/user-data';
import { ActionSheetController } from '@ionic/angular';
import * as urls from '../../providers/urls.servicios';
import { Platform } from '@ionic/angular';
import { Ubicacion } from '../../providers/ubicacion';
import { Valores } from '../../providers/valores';
import { RegistroUsuariosService } from '../../providers/registro-usuarios.service';

@Component({
  selector: 'page-schedule',
  templateUrl: 'schedule.html',
  styleUrls: ['./schedule.scss'],
})
export class SchedulePage implements OnInit, AfterViewInit {
  // Gets a reference to the list element
  @ViewChild('mapCanvas') mapElement: ElementRef;
  @ViewChild('scheduleList')
  scheduleList: IonList;

  dayIndex = 0;
  queryText = '';
  segment = 'all';
  excludeTracks: any = [];
  shownSessions: any = [];
  groups: any = [];
  confDate: string;
  URLAvatar = urls.URLimagen;
  public stories = [
    {
      id: 1,
      img: 'assets/img/clasificados/beer-company-bogota.jpg',
      user_name: 'Cerveza BBC'
    },
    {
      id: 2,
      img: 'assets/img/clasificados/heineken-logo.jpg',
      user_name: 'Heineken'
    },
    {
      id: 3,
      img: 'assets/img/clasificados/bavaria.png',
      user_name: 'Bavaria'
    },
    {
      id: 4,
      img: 'assets/img/clasificados/postobon.png',
      user_name: 'Postobon'
    },
    {
      id: 5,
      img: 'assets/img/clasificados/duff.jpg',
      user_name: 'Duff'
    },
    {
      id: 6,
      img: 'assets/img/clasificados/miller.png',
      user_name: 'Miller'
    },
    {
      id: 7,
      img: 'assets/img/clasificados/Disco-Coca-Cola-v1.jpg',
      user_name: 'Coca-Cola'
    },
    {
      id: 8,
      img: 'https://avatars1.githubusercontent.com/u/1024025?v=3&s=120',
      user_name: 'linus_torvalds'
    }

  ];

  constructor(
    public alertCtrl: AlertController,
    public actionSheetCtrl: ActionSheetController,
    public confData: ConferenceData,
    public loadingCtrl: LoadingController,
    public modalCtrl: ModalController,
    public router: Router,
    public toastCtrl: ToastController,
    public user: UserData,
    public platform: Platform,
    public ubicacion: Ubicacion,
    public valor: Valores,
    public registro: RegistroUsuariosService
  ) { }

  ngOnInit() {
    this.registro.listarEventos(this.valor.identificacion);
  }

  
  async seleccionDireccion() {
    const actionSheet = await this.actionSheetCtrl.create({
      header: 'Cambiar tu dirección',
      mode: 'ios',
      buttons: [
        {
          text: 'Carrera 13 # 142-60 (Actual)',
          icon: 'checkmark'
        },
        {
          text: 'Carrera 74A # 26-10 (Oficina)'
        },
        {
          text: 'Calle 19 # 14-51'
        },
         {
          text: 'Cra. 72K # 38B-01 Sur'
        },
        {
          text: 'Ubicación GPS',
          icon: 'locate'
        }
      ]
    });

    await actionSheet.present();
  }
  aDondeIr() {
    console.log('ADondeIr cliqueado!');
  }
  evento1() {
    console.log('evento1 cliqueado!');
  }

  evento2() {
    console.log('evento2 cliqueado!');
  }
  evento3() {
    console.log('evento3 cliqueado!');
  }

  evento4() {
    console.log('evento4 cliqueado!');
  }
  evento5() {
    console.log('evento5 cliqueado!');
  }

  evento6() {
    console.log('evento6 cliqueado!');
  }


  async cargarTarjetaMapa(lat, lng) {
    
    if (lat && lng) {
      console.log('hay latitud y lingitud, se carga gmaps');
      const googleMaps = await getGoogleMaps(
        'AIzaSyB8pf6ZdFQj5qw7rc_HSGrhUwQKfIe9ICw'
      );

      const mapEle = this.mapElement.nativeElement;
      const map = new googleMaps.Map(mapEle, {
        center: {
          'name': 'Mi ubicación',
          'lat': parseFloat(this.ubicacion.latitud),
          'lng': parseFloat(this.ubicacion.longitud),
          'center': true
        },
        zoom: 17
      });
      const center = googleMaps.ILatLng = {
        'lat': parseFloat(this.ubicacion.latitud),
        'lng': parseFloat(this.ubicacion.longitud)
      };

      const infoWindow = new googleMaps.InfoWindow({
        content: `<ion-avatar><img src="${this.URLAvatar}${this.valor.identificacion}.jpg" alt="Mi ubicación"></ion-avatar>
                      `
      });

      // console.log('markerdata: ' + JSON.stringify(markerData));
      const marker = new googleMaps.Marker({
        position: {
          'lat': parseFloat(this.ubicacion.latitud),
          'lng': parseFloat(this.ubicacion.longitud)
        },
        map,
        title: 'Mi ubicación',
        icon: './assets/img/blue-marker.png'
      });


      marker.addListener('click', () => {
        infoWindow.open(map, marker);
      });


      googleMaps.event.addListenerOnce(map, 'idle', () => {
        mapEle.classList.add('show-map');
        infoWindow.open(map, marker);
      });


      setTimeout(() => {


        /*      setTimeout(() => {
       
               console.log('mi identificacion en el mapa es:', this.valor.identificacion);
       
               this.registro.PersonasCercanas.cercanos.forEach((markerData: any) => {
       
                 const infoWindow = new googleMaps.InfoWindow({
                   content: `<div [hidden]="${this.valor.identificacion}==${markerData.Identificacion}" >
                                 <ion-chip (click)="detalles(${markerData.Identificacion})" outline="true">
                                   <ion-avatar>
                                     <img src="http://186.30.53.158/php-crud/usuarios/${markerData.foto_perfil}">
                                   </ion-avatar>
                                     <ion-label> <strong>${markerData.Cargo_Actual}</strong> </ion-label>
                                     <ion-icon name="person-add"></ion-icon>
                                 </ion-chip>
                             </div>
                             `
                 });
                 console.log('markerdata: ' + JSON.stringify(markerData));
                 const marker = new googleMaps.Marker({
                   position: {
                     'lat': parseFloat(markerData.LatitudActual),
                     'lng': parseFloat(markerData.LongitudActual)
                   },
                   map,
                   title: markerData.Nombre,
                   icon: './assets/img/puntoGps.png'
                 });
       
                 setTimeout(() => {
                   marker.addListener('click', () => {
                     infoWindow.open(map, marker);
                   });
                 });
       
                 googleMaps.event.addListenerOnce(map, 'idle', () => {
                   mapEle.classList.add('show-map');
                 });
               }, 100);
       
       
             }, 1500); */




      }, 100);

    }

  }
  
  ngAfterViewInit() { 
    console.log('entra al ngAfter');
    this.ubicacion.iniciarGeubicacion();
    setTimeout(() => {
      this.cargarTarjetaMapa(this.ubicacion.latitud, this.ubicacion.longitud);
    }, 5000);
   
    
  }


} // fin de la clase

function getGoogleMaps(apiKey: string): Promise<any> {
  const win = window as any;
  const googleModule = win.google;
  if (googleModule && googleModule.maps) {
    return Promise.resolve(googleModule.maps);
  }

  return new Promise((resolve, reject) => {
    const script = document.createElement('script');
    script.src = `https://maps.googleapis.com/maps/api/js?v=weekly&key=${apiKey}&v=3.37`;
    script.async = true;
    script.defer = true;
    document.body.appendChild(script);
    script.onload = () => {
      const googleModule2 = win.google;
      if (googleModule2 && googleModule2.maps) {
        resolve(googleModule2.maps);
      } else {
        reject('google maps not available');
      }
    };
  });
}

