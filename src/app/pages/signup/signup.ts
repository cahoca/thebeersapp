import { Component, ViewEncapsulation } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';

import { UserData } from '../../providers/user-data';

import { RegistroUsuariosService } from '../../providers/registro-usuarios.service';

import { UserOptionsRegistration } from '../../interfaces/user-options-registration';
import { Valores } from '../../providers/valores';
import { LoadingController, ToastController, ActionSheetController } from '@ionic/angular';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import * as urls from '../../providers/urls.servicios';



@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
  styleUrls: ['./signup.scss'],
})
export class SignupPage {
 // signup: UserOptions = { username: '', password: '' };
  signup: UserOptionsRegistration = {
    name: '',
    surname: '',
    ID: '',
    email: '',
    celular: '',
    password: '',
    PIN: '',
    genere: '',
    direccion: '',
    ciudad: ''
  };
  submitted = false;
  emailInvalido: boolean = false;
  fototomada = false;
  fotografia: any;
  constructor(
    public router: Router,
    public userData: UserData,
    public registro: RegistroUsuariosService,
    public valor: Valores,
    public loadingController: LoadingController,
    public toastController: ToastController,
    public actionSheetController: ActionSheetController,
     private camera: Camera,
    private transfer: FileTransfer
  ) {}

  async presentLoadingWithOptions(tiempo) {
    const loading = await this.loadingController.create({
      duration: tiempo,
      message: 'Un momento, por favor...',
      translucent: true,
      cssClass: 'custom-class custom-loading'
    });
    return await loading.present();
  }

  preregistro() {
    // SI SE LLEGA AQUÍ ES PORQUE LA VALIDACIÓN DE DATOS ESÁ COMPLETA Y SE PROCEDE A PREREGISTRAR AL USUARIO
    this.registro.preregistrar(
      this.signup.name,
      this.signup.surname,
      this.signup.ID,
       this.signup.celular,
      this.signup.email,
      this.signup.password
    ).subscribe((rta) => {
      console.log(rta['mensaje']);

      if (rta['mensaje'] !== 'Ya existe un usuario con ese número de identificación') {
        this.valor.guardarid(this.signup.ID);
        this.valor.correo = this.signup.email;
        this.presentLoadingWithOptions(2000);
        setTimeout(() => {
          this.router.navigateByUrl('/login');
        }, 1800);
      }
      else {
        this.presentarToastError();
      }
    });
  }

  async presentarToastError() {
    const toast = await this.toastController.create({
      message: 'Ya existe un usuario registrado',
      showCloseButton: true,
      position: 'bottom',
      closeButtonText: 'Listo',
      color: 'danger'
    });
    toast.present();
  }

  async updatePicture() {
    console.log('Clicked to update picture');
    const actionSheet = await this.actionSheetController.create({
      header: 'Editar la foto de perfil',
      buttons: [{
        text: 'Tomar una foto',
        role: 'destructive',
        icon: 'camera',
        handler: () => {
          console.log('Camera clicked');
          this.tomarFoto();
        }
      }, {
        text: 'Subir una foto',
        icon: 'images',
        handler: () => {
          console.log('Subir clicked');
          this.GaleriaImagen();
        }
      }, {
        text: 'Cancelar',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
  }

  tomarFoto() {
    let options: CameraOptions = {
      quality: 95,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      saveToPhotoAlbum: true,
      allowEdit: true,
      targetWidth: 600,
      targetHeight: 600
    };

    this.camera.getPicture(options).then(
      imageData => {
        // imageData is either a base64 encoded string or a file URI
        // If it's base64:
        this.fotografia = 'data:image/jpeg;base64,' + imageData;
        setTimeout(() => {
         // this.SubirFoto();
          this.fototomada = true;
        }, 500);

      },
      err => {
        this.fototomada = false;
        // Handle error
      }
    );
  }

  GaleriaImagen() {
    let options: CameraOptions = {
      quality: 95,
      destinationType: this.camera.DestinationType.DATA_URL,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      saveToPhotoAlbum: true,
      allowEdit: true,
      targetWidth: 600,
      targetHeight: 600
    };

    this.camera.getPicture(options).then(
      imageData => {
        // imageData is either a base64 encoded string or a file URI
        // If it's base64:
        this.fotografia = 'data:image/jpeg;base64,' + imageData;
        setTimeout(() => {

          this.fototomada = true;
        //  this.SubirFoto();
        }, 500);
      },
      err => {
        // Handle error
        this.fototomada = false;
      }
    );
  }

  SubirFoto() {
    // create file transfer object
    const fileTransfer: FileTransferObject = this.transfer.create();
    console.log('direccion: ' + urls.fotos + 'uploadPhoto.php');

    // option transfer
    let options: FileUploadOptions = {
      fileKey: 'photo',
      fileName: this.signup.ID + '.jpg',
      chunkedMode: false,
      httpMethod: 'post',
      mimeType: 'image/jpeg',
      headers: {}
    };

    // file transfer action
    fileTransfer
      .upload(this.fotografia, urls.fotos + 'uploadPhoto.php', options)
      .then(
        data => {
          // alert('Correcto');
         // this.userData.inicio();
        },
        err => {
          console.log(err);
         // this.presentarToastFoto();
          // alert('Incorrecto');
        }
      );
  }


  async selcccionarGenero() {
    console.log('Clicked to update picture');
    const actionSheet = await this.actionSheetController.create({
      header: 'Selecciona tu género',
      buttons: [{
        text: 'Mujer',
        role: 'destructive',
        icon: 'woman',
        handler: () => {
          console.log('Mujer clicked');
          this.signup.genere = 'Mujer';
          //this.tomarFoto();
        }
      }, {
        text: 'Hombre',
        icon: 'man',
        handler: () => {
          console.log('Hombre clicked');
          this.signup.genere = 'Hombre';
          //this.GaleriaImagen();
        }
      }, {
        text: 'Cancelar',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
  }

  completarRegistro() {
    if (this.fototomada == true) {
      this.SubirFoto();
    }
    this.registro.completarRegistro(this.signup.genere,
                                    this.signup.direccion,
                                    this.signup.ciudad,
                                    this.signup.ID);
    this.presentarhaciendomagia();
    setTimeout(() => {
      this.router.navigateByUrl('/welcome-page');
    }, 5000);
  }

  async presentarhaciendomagia() {
    const loading = await this.loadingController.create({
      duration: 5000,
      message: 'Estamos haciendo magia... un momento',
      translucent: true,
      cssClass: 'custom-class custom-loading'
    });
    return await loading.present();
  }

  onSignup(form: NgForm) {
    this.submitted = true;

    if (form.valid) {
      let indiceArroba = this.signup.email.indexOf('@');
      if (
        indiceArroba == -1 ||
        indiceArroba == 0 ||
        indiceArroba == this.signup.email.length - 1
      ) {
        this.emailInvalido = true;
        return;
      } else {
        let indiceOtroArroba = this.signup.email.indexOf('@', indiceArroba + 1);
        if (indiceOtroArroba === -1) {
          this.emailInvalido = false;
        } else {
          this.emailInvalido = true;
          return;
        }
        this.registro.registrar(this.signup.name,
                                this.signup.surname,
                                this.signup.ID,
                                this.signup.celular,
                                this.signup.email,
                                this.signup.password);
          this.presentLoadingWithOptions(3000);
      }
      // this.mostrarAlertConfirmacion();
    }
  }
}
