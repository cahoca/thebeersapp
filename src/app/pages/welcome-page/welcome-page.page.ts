import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'welcome-page',
  templateUrl: './welcome-page.page.html',
  styleUrls: ['./welcome-page.page.scss'],
})
export class WelcomePagePage implements OnInit {

  constructor(
    public router: Router
  ) { }

  ngOnInit() {
  }

  login() {
    this.router.navigateByUrl('/login');
  }

}
