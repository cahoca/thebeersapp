import { Component, ViewEncapsulation } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';

import { UserData } from '../../providers/user-data';


import { UserOptions } from '../../interfaces/user-options';
import { AlertController } from '@ionic/angular';
import { IngresoPlataformaService } from '../../providers/ingreso-plataforma.service';
import { Valores } from '../../providers/valores';
import { LoginValidation } from '../../interfaces/login-validation';
import { RegistroUsuariosService } from '../../providers/registro-usuarios.service';
import { AppComponent } from '../../app.component';



@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
  styleUrls: ['./login.scss'],
})
export class LoginPage {
  // login: UserOptions = { username: '', password: '' };
  login: UserOptions = {
    username: '',
    password: '',
    tel_cel: '',
    email: '',
    bornDate: '',
    id_user: ''
  };
  submitted = false;
  accesoDenegado: boolean = false;

  constructor(
    public userData: UserData,
    public router: Router,
    public alertController: AlertController,
    public loginAccount: IngresoPlataformaService,
    public valor: Valores,
    public registro: RegistroUsuariosService,
    public componenteApp: AppComponent,


  ) { }

  onLogin(form: NgForm) {
    this.submitted = true;

    if (form.valid) {

      this.loginAccount.validarDatos(this.login.username, this.login.password, this.valor.UUID)
        .subscribe((rta: LoginValidation) => {

          console.log(rta);

          if (!rta.error) {

            this.valor.identificacion = rta.id_usuario;
            this.userData.setUserID(rta.id_usuario);
           /*  setTimeout(() => {
              this.registro.contrasenaCambiada('0', rta.id_usuario);
            }, 200); */
            this.componenteApp.sesiones();
            this.userData.login();
            // this.userData.login(this.login.username);
            this.componenteApp.presentLoading();
            setTimeout(() => {
              this.router.navigateByUrl('/app/tabs/schedule');
            }, 1500);
          } else {
            this.accesoDenegado = true;
            console.log('acceso denegado');
          }

        });
    }
  }

  onSignup() {
    this.router.navigateByUrl('/signup');
  }
}
