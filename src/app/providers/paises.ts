import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { UserData } from './user-data';

@Injectable({
  providedIn: 'root'
})
export class Paises {
  public paises: any;

  constructor(public http: HttpClient,
              public user: UserData) {}

  Paises() {
    return this.http.get('./assets/data/codigosNumeros.json');
  }

  async cargarPaises() {
    setTimeout(() => {
      this.Paises().subscribe(
        dato => {
          this.paises = dato['codigos'];
          console.log(this.paises);
        },
        error => {
          console.error(error);
        }
      );
    }, 500);


  }

}
