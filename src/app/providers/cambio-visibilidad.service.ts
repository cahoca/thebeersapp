import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { URL_thebeersapp1 } from './urls.servicios';

@Injectable({
  providedIn: 'root'
})
export class RegistroUsuariosService {
  public RTA: any; 

  constructor(private http: HttpClient) {
    console.log(
      'El servicio de registro está bien configurado y listo para ser usado'
    );
  }

  preregistrar(
    user: string,
    telCel: string,
    email: string,
    bornDate: string,
    id_user: string
  ): any {
    const datosPreRegistro = {
      user: user,
      telCel: telCel,
      email: email,
      bornDate: bornDate,
      id_user: id_user
    };

    // ESTE METODO GET ES SOLO UNA PRUEBA, DESCOMENTAR ENCASO DE QUERER REALIZAR LA PRUEBA (TIEMPO DE DESARROLLO)
    /*this.http.get(URL_thebeersapp1 + '/Prueba/obtenerDatoBD/' + user + '/' + telCel + '/' + email + '/' + bornDate)
     .subscribe(respuesta =>{
       console.log(respuesta);
     });*/

    // ESTE POST HACE EL PREREGISTRO EN LA BASE DE DATOS
    this.http
      .post(URL_thebeersapp1 + '/PreRegistroUsuario', datosPreRegistro)
      .subscribe(respuesta => {
        console.log(respuesta);
        this.RTA = respuesta;
      });
  }

  registrar(
    Tipo_Ocupacion: string,
    Cargo_Actual: string,
    Direccion_Residencia: string,
    Perfil: string,
    Nivel_Academico: string,
    Twitter: string,
    Instagram: string,
    Linkedin: string,
    Github: string,
    WWW: string,
    Profesion: string,
    Institucion_Educativa: string,
    Ha_Trabajado: string,
    Cuantos_Idiomas: string,
    id_user: string
  ): any {
    const datosRegistro = {
      Tipo_Ocupacion: Tipo_Ocupacion,
      Cargo_Actual: Cargo_Actual,
      Direccion_Residencia: Direccion_Residencia,
      Perfil: Perfil,
      Nivel_Academico: Nivel_Academico,
      Twitter: Twitter,
      Instagram: Instagram,
      Linkedin: Linkedin,
      Github: Github,
      WWW: WWW,
      Profesion: Profesion,
      Institucion_Educativa: Institucion_Educativa,
      Ha_Trabajado: Ha_Trabajado,
      Cuantos_Idiomas: Cuantos_Idiomas,
      id_user: id_user
    };

    // ESTE POST HACE EL PREREGISTRO EN LA BASE DE DATOS
    this.http
      .post(URL_thebeersapp1 + '/RegistroUsuario', datosRegistro)
      .subscribe(respuesta => {
        console.log(respuesta);
        this.RTA = respuesta;
      });
  }

  registroProfesion(Profesion: string, Descripcion: string): any {
    const datosRegistroProfesion = {
      Profesion: Profesion,
      Descripcion: Descripcion
    };

    // ESTE POST HACE EL PREREGISTRO EN LA BASE DE DATOS
    this.http
      .post(
        URL_thebeersapp1 + '/RegistroProfesiones',
        datosRegistroProfesion
      )
      .subscribe(respuesta => {
        console.log(respuesta);
        this.RTA = respuesta;
      });
  }

  registroEduucacion(
    Nivel_Academico: string,
    Institucion_Educativa: string,
    Identificacion: string,
    Titulo_Obtenido: string,
    Ciudad: string,
    Fecha_Inicio: string,
    Fecha_Fin: string
  ): any {
    const datosRegistroEducacion = {
      Nivel_Academico: Nivel_Academico,
      Institucion_Educativa: Institucion_Educativa,
      Identificacion: Identificacion,
      Titulo_Obtenido: Titulo_Obtenido,
      Ciudad: Ciudad,
      Fecha_Inicio: Fecha_Inicio,
      Fecha_Fin: Fecha_Fin
    };

    // ESTE POST HACE EL PREREGISTRO EN LA BASE DE DATOS
    this.http
      .post(
        URL_thebeersapp1 + '/RegistroFormacionAcademica',
        datosRegistroEducacion
      )
      .subscribe(respuesta => {
        console.log(respuesta);
        this.RTA = respuesta;
      });
  }

  registroEmpresas(
    RazonSocial: string,
    Direccion: string,
    Telefonos: string,
    Correos: string,
    Ciudad: string
  ): any {
    const datosRegistroEmpresas = {
      RazonSocial: RazonSocial,
      Direccion: Direccion,
      Telefonos: Telefonos,
      Correos: Correos,
      Ciudad: Ciudad
    };

    // ESTE POST HACE EL PREREGISTRO EN LA BASE DE DATOS
    this.http
      .post(URL_thebeersapp1 + '/RegistroEmpresas', datosRegistroEmpresas)
      .subscribe(respuesta => {
        console.log(respuesta);
        this.RTA = respuesta;
      });
  }

  registroExperiencia(
    Identificacion: string,
    Cargo: string,
    Empresa: string,
    Funciones_Cargo: string,
    Fecha_Inicio: string,
    Fecha_Fin: string,
    Telefonos: string
  ): any {
    const datosRegistroExperiencia = {
      Identificacion: Identificacion,
      Cargo: Cargo,
      Empresa: Empresa,
      Funciones_Cargo: Funciones_Cargo,
      Fecha_Inicio: Fecha_Inicio,
      Fecha_Fin: Fecha_Fin,
      Telefonos: Telefonos
    };

    // ESTE POST HACE EL PREREGISTRO EN LA BASE DE DATOS
    this.http
      .post(
        URL_thebeersapp1 + '/RegistroExperienciasLaborales',
        datosRegistroExperiencia
      )
      .subscribe(respuesta => {
        console.log(respuesta);
        this.RTA = respuesta;
      });
  }

  registroHabilidad(Aptitud: string, Descripcion: string): any {
    const datosHabilidad = {
      Aptitud: Aptitud,
      Descripcion: Descripcion
    };

    // ESTE POST HACE EL PREREGISTRO EN LA BASE DE DATOS
    this.http
      .post(URL_thebeersapp1 + '/RegistroAptitudes', datosHabilidad)
      .subscribe(respuesta => {
        console.log(respuesta);
        this.RTA = respuesta;
      });
  }

  registroIdioma(
    Certificado: string,
    Identificacion: string,
    idioma: string,
    Nivel: string
    ): any {
    const datosIdioma = {
      Certificado: Certificado,
      Identificacion: Identificacion,
      idioma: idioma,
      Nivel: Nivel
    };

    // ESTE POST HACE EL PREREGISTRO EN LA BASE DE DATOS
    this.http
      .post(URL_thebeersapp1 + '/RegistroIdiomas', datosIdioma)
      .subscribe(respuesta => {
        console.log(respuesta);
        this.RTA = respuesta;
      });
  }
}
