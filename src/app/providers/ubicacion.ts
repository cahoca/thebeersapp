
import { Injectable } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation/ngx'; 
import { LoadingController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class Ubicacion {
  data: any;
  activarGPS: boolean = false;
  longitud: any;
  latitud: any;
  precision: any;
  watch: any;

  constructor(public geoubicacion: Geolocation, public loadingController: LoadingController ) {
    console.log('El servicio de ubicación está activado');
  }

  iniciarGeubicacion() {
    const options = {
      timeout: 99000,
      enableHighAccuracy: true,
      maximumAge: 0
    };
    this.watch = this.geoubicacion.getCurrentPosition(options).then((resp) => {
      this.longitud = resp.coords.longitude;
      this.latitud = resp.coords.latitude;
      this.precision = resp.coords.accuracy;
      this.activarGPS = true;
    }).catch((error) => {
      this.activarGPS = false;
      this.iniciarGeubicacion();
    });
  }
  async presentLoadingWithOptions() {
    const loading = await this.loadingController.create({
      spinner: 'dots',
      message: 'Obteniendo coordenadas',
      translucent: true,
      cssClass: 'custom-class custom-loading'
    });
    return await loading.present();
  }
  ubicacionInstantanea() {
    const options = {
      timeout: 99000,
      enableHighAccuracy: true
    };
    this.watch = this.geoubicacion.watchPosition(options);
    this.watch.subscribe((data) => {
      this.longitud = data.coords.longitude;
      this.latitud = data.coords.latitude;
      this.activarGPS = true;
      console.log('Ubicación al instante recibida');
    },
    (err) => {
      this.activarGPS = false;
      console.log('Error getting location', err);
    });
  }

  cancelarUbicacion() {
    try {
      this.watch.unsubscribe();
    } catch (e) {
      console.log(JSON.stringify(e));
    }
  }

}
