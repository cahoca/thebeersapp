import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as urls from '../providers/urls.servicios';


@Injectable({
  providedIn: 'root'
})
export class Actualizaciones {
 

  constructor(public http: HttpClient) {}

  cargarVersionNueva() {
    return this.http.get(urls.URLphpCrud + 'versionactual/1');
  }
 
}
