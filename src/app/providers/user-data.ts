import { Injectable } from '@angular/core';
import { Events } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { Valores } from '../providers/valores';
import { Platform } from '@ionic/angular';
import { Actualizaciones } from './actualizaciones';

@Injectable({
  providedIn: 'root'
})
export class UserData {
  _favorites: string[] = [];
  HAS_LOGGED_IN = 'hasLoggedIn';
  HAS_SEEN_TUTORIAL = 'hasSeenTutorial';
  public datos: any = [{
    Nombres: '',
    Apellidos: '',
    activo: '',
    foto_perfil: '',
    Telefono: '',
    ContrasenaCambiada: '',
    Puntos: ''
  }];
  public datosactualizacion: any = [{
    nombre: '',
    versionNumber: '',
    version: '',
    fechaCompilacion: '',
    url: ''
  }];
  InicioSesion: boolean;

  constructor(
    public events: Events,
    public storage: Storage,
    public valor: Valores,
    private plataforma: Platform,
    public actualizaciones: Actualizaciones
  ) { }

  hasFavorite(sessionName: string): boolean {
    return (this._favorites.indexOf(sessionName) > -1);
  }
 

  cargarDatosActualizacion() {
    setTimeout(() => {
      this.actualizaciones.cargarVersionNueva().subscribe((dat: any[]) => {
        this.datosactualizacion = dat;
      },
        error => {
          console.error(error);
        }
      );
    }, 500);
  }

  inicio() {
    setTimeout(() => {
      this.valor.cargar_id().subscribe((data: any[]) => {
        this.datos = data;
        this.InicioSesion = true;
        console.log(JSON.stringify(this.datos));
      },
      (err: any[]) => {
        this.InicioSesion = true;
        this.getUsername();
       // console.log(JSON.stringify(this.datos));
      });
    }, 100);

  } 

  addFavorite(sessionName: string): void {
    this._favorites.push(sessionName);
  }

  removeFavorite(sessionName: string): void {
    const index = this._favorites.indexOf(sessionName);
    if (index > -1) {
      this._favorites.splice(index, 1);
    }
  }

  login() {
    setTimeout(() => {
      this.setUsername(this.datos.Nombre);
      console.log('datos almacenados correctamente en la base de datos');
    }, 700);
  }

  logout() {
         this.storage.remove('Identificacion');
         this.storage.remove('Nombre');
      }

  setUsername(userName: string) {
      return this.storage.set('Nombre', userName);
  }

getUsername() {
  this.storage.get('Nombre').then((id) => {
    this.datos.Nombre = id;
  }).catch((error: any) => this.InicioSesion = false );
  }

  getUserID() {
    this.storage.get('Identificacion').then((id) => {
      this.valor.identificacion = id;
    });
  }

  setUserID(id: string) {
      return this.storage.set('Identificacion', id);
  }
}
