import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';

import * as urls from '../providers/urls.servicios';

@Injectable({
  providedIn: 'root'
})
export class Valores {
  data: any;
  identificacion: string = null;
  UUID: any;
  correo: any;

  constructor(public http: HttpClient
              ) {}

  cargar_id(): any {
    console.log('Los datos de ID para cargar son: ');
    console.log(this.identificacion);
    console.log('Entonces, la dirección será: ');
    console.log(urls.URLphpCrud + 'usuarios/' + this.identificacion);
    if (this.data) {
      return of(this.data);
    } else {
      return this.http
        .get(urls.URLphpCrud + 'usuarios/' + this.identificacion);
       // .pipe(map(this.processData, this));
    }
  }

  guardarid(identifica: string) {
     this.identificacion = identifica;/* 
     console.log('Se almacenaron los datos de ID con: ');
    console.log(this.identificacion); */
  }

  getNuevo() {
    return this.cargar_id().pipe(
      map((data: any) => {
        return data.records.sort((a: any, b: any) => {
          const aName = a.Nombre.split(' ').pop();
          const bName = b.Nombre.split(' ').pop();
          return aName.localeCompare(bName);
        });
      })
    );
  }

}
