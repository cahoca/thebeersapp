import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { URL_thebeersapp1 } from './urls.servicios';
import * as urls from './urls.servicios';
import { Valores } from './valores';

@Injectable({
  providedIn: 'root'
})
export class RegistroUsuariosService {
  public RTA: any;
  public RTARegistro: any = [{
    eventos: ''
  }];
  public RTARegistroC: any = [{
    mensaje: ''
  }];
  public Eventos: any = [{
    mensaje: ''
  }];
  public PINGenerado: any = [{
    mensaje: '',
    PINAsignado: ''
  }];
  public PINLlegado: any = [{
    mensaje: ''
  }];
  public PersonasCercanas: any = [{
    cercanos: ''
  }];
  public Repartidas: any = [{
    Personas: ''
  }];
  public Amigos: any = [{
    amigos: ''
  }];
  public AmigosEnComun: any = [{
    amigoscomun: ''
  }];
  public SolicitudComprobada: any = [{
    solicitudenviada: ''
  }];
  verificacionContrasena = false;
  public solicitudesPendientes: any = [{
    solicitudes: '',
    cantidad: ''
  }];
  public CargaAmigosCompleta: boolean;
  public CargaAmigosComunCompleta: boolean;
  public RTA_Clasificado: boolean = false;
  public RTA_Feria_evento: boolean = false;
  public Contrasena: boolean;
  public ContactoCorrecto = false;
  public IDRetornado: any = [{
    Identificacion: ''
  }];
  constructor(private http: HttpClient,
              public valor: Valores) {
    // console.log('El servicio de registro está bien configurado y listo para ser usado');
  }

  preregistrar(
    name: string,
    surname: string,
    ID: string,
    celular: string,
    email: string,
    password: string,
  ): any {
    const datosPreRegistro = {
      name: name,
      surname: surname,
      ID: ID,
      celular: celular,
      email: email,
      password: password
    };

    // ESTE POST HACE EL PREREGISTRO EN LA BASE DE DATOS
    return this.http
      .post(URL_thebeersapp1 + '/PreRegistroUsuario', datosPreRegistro);
  }

  preregistroEmpresa(
    enterprisename: string,
    nit_enterprise: string,
    Enterprise_tel_cel: string,
    Enterpriseemail: string,
    ciiu: string
  ): any {
    const datosPreRegistro = {
      enterprisename: enterprisename,
      nit_enterprise: nit_enterprise,
      Enterprise_tel_cel: Enterprise_tel_cel,
      Enterpriseemail: Enterpriseemail,
      ciiu: ciiu
    };

    // ESTE POST HACE EL PREREGISTRO EN LA BASE DE DATOS
    this.http
      .post(URL_thebeersapp1 + '/PreRegistroEmpresa', datosPreRegistro)
      .subscribe(respuesta => {
        // console.log(respuesta);
        this.RTA = respuesta;
      }, (err: any) => {
          this.RTA = 'error';
          // console.log(this.RTA);
      });
  }

  registrar(
    Nombre: string,
    apellidos: string,
    identificacion: string,
    celular: string,
    email: string,
    contrasena: string

  ): any {
    const datosRegistro = {
     Nombre: Nombre,
     apellidos: apellidos,
     identificacion: identificacion,
     celular: celular,
     email: email,
     contrasena: contrasena
    };

    // ESTE POST HACE EL PREREGISTRO EN LA BASE DE DATOS
    this.http
      .post(URL_thebeersapp1 + '/RegistroUsuario', datosRegistro)
      .subscribe(respuesta => {
        console.log(respuesta);
        this.RTARegistro = respuesta;
      });
  }

  completarRegistro(
    genere: string,
    direccion: string,
    ciudad: string,
    identificacion: string

  ): any {
    const datosRegistro = {
      genere: genere,
      direccion: direccion,
      ciudad: ciudad,
      identificacion: identificacion
    };

    // ESTE POST HACE EL PREREGISTRO EN LA BASE DE DATOS
    this.http
      .post(URL_thebeersapp1 + '/CompletarRegistro', datosRegistro)
      .subscribe(respuesta => {
        console.log(respuesta);
        this.RTARegistroC = respuesta;
      });
  }

  registroProfesion(Profesion: string, Descripcion: string): any {
    const datosRegistroProfesion = {
      Profesion: Profesion,
      Descripcion: Descripcion
    };

    // ESTE POST HACE EL PREREGISTRO EN LA BASE DE DATOS
    this.http
      .post(
        URL_thebeersapp1 + '/RegistroProfesiones',
        datosRegistroProfesion
      )
      .subscribe(respuesta => {
        // console.log(respuesta);
        this.RTA = respuesta;
      });
  }

  registroEduucacion(
    Nivel_Academico: string,
    Institucion_Educativa: string,
    Identificacion: string,
    Titulo_Obtenido: string,
    Ciudad: string,
    Fecha_Inicio: string,
    Fecha_Fin: string
  ): any {
    const datosRegistroEducacion = {
      Nivel_Academico: Nivel_Academico,
      Institucion_Educativa: Institucion_Educativa,
      Identificacion: Identificacion,
      Titulo_Obtenido: Titulo_Obtenido,
      Ciudad: Ciudad,
      Fecha_Inicio: Fecha_Inicio,
      Fecha_Fin: Fecha_Fin
    };

    // ESTE POST HACE EL PREREGISTRO EN LA BASE DE DATOS
    this.http
      .post(
        URL_thebeersapp1 + '/RegistroFormacionAcademica',
        datosRegistroEducacion
      )
      .subscribe(respuesta => {
        // console.log(respuesta);
        this.RTA = respuesta;
      });
  }

  listarEventos(
    Identificacion: string
  ): any {
    const datos = {
      Identificacion: Identificacion
    };

    // ESTE POST HACE EL PREREGISTRO EN LA BASE DE DATOS
    this.http
      .post(
        URL_thebeersapp1 + '/ListarEventos',
        datos
      )
      .subscribe(respuesta => {
        // console.log(respuesta);
        this.Eventos = respuesta;
      });
  }

  registroEmpresas(
    NIT: string,
    RazonSocial: string,
    Direccion: string,
    Telefonos: string,
    Correos: string,
    Ciudad: string
  ): any {
    const datosRegistroEmpresas = {
      NIT: NIT,
      RazonSocial: RazonSocial,
      Direccion: Direccion,
      Telefonos: Telefonos,
      Correos: Correos,
      Ciudad: Ciudad
    };

    // ESTE POST HACE EL PREREGISTRO EN LA BASE DE DATOS
    this.http
      .post(URL_thebeersapp1 + '/RegistroEmpresas', datosRegistroEmpresas)
      .subscribe(respuesta => {
        // console.log(respuesta);
        this.RTA = respuesta;
      });
  }

  registroTipoPerfil(
    Identificacion: string,
    Tipo_Ocupacion: string
  ): any {
    const datosRegistroTipo = {
      Identificacion: Identificacion,
      Tipo_Ocupacion: Tipo_Ocupacion
    };

    // ESTE POST HACE EL PREREGISTRO EN LA BASE DE DATOS
    this.http
      .post(URL_thebeersapp1 + '/RegistroTipoUsr', datosRegistroTipo)
      .subscribe(respuesta => {
        // console.log(respuesta);
        this.RTA = respuesta;
      });
  }

  registroExperiencia(
    Identificacion: string,
    Cargo: string,
    Empresa: string,
    Funciones_Cargo: string,
    Fecha_Inicio: string,
    Fecha_Fin: string,
    Telefono1: string,
    Telefono2: string
  ): any {
    const datosRegistroExperiencia = {
      Identificacion: Identificacion,
      Cargo: Cargo,
      Empresa: Empresa,
      Funciones_Cargo: Funciones_Cargo,
      Fecha_Inicio: Fecha_Inicio,
      Fecha_Fin: Fecha_Fin,
      Telefono1: Telefono1,
      Telefono2: Telefono2
    };

    // ESTE POST HACE EL PREREGISTRO EN LA BASE DE DATOS
    this.http
      .post(
        URL_thebeersapp1 + '/RegistroExperienciasLaborales',
        datosRegistroExperiencia
      )
      .subscribe(respuesta => {
        // console.log(respuesta);
        this.RTA = respuesta;
      });
  }

  registroHabilidad(
    Aptitud: string,
    Descripcion: string,
    Identificacion: string,
    ):
    any {
    const datosHabilidad = {
      Aptitud: Aptitud,
      Descripcion: Descripcion,
      Identificacion: Identificacion
    };

    // ESTE POST HACE EL PREREGISTRO EN LA BASE DE DATOS
    this.http
      .post(URL_thebeersapp1 + '/RegistroAptitudes', datosHabilidad)
      .subscribe(respuesta => {
        // console.log(respuesta);
        this.RTA = respuesta;
      });
  }

  registroReferencias(
    Tipo: string,
    Nombre_Referencia: string,
    Ocupacion: string,
    Telefono_Celular: string,
    Identificacion: string,
  ):
    any {
    const datosRef = {
      Tipo: Tipo,
      Nombre_Referencia: Nombre_Referencia,
      Ocupacion: Ocupacion,
      Telefono_Celular: Telefono_Celular,
      Identificacion: Identificacion
    };

    // ESTE POST HACE EL PREREGISTRO EN LA BASE DE DATOS
    this.http
      .post(URL_thebeersapp1 + '/RegistroReferencias', datosRef)
      .subscribe(respuesta => {
        // console.log(respuesta);
        this.RTA = respuesta;
      });
  }

  registroIdioma(
    Certificado: string,
    Identificacion: string,
    idioma: string,
    Nivel: string
  ): any {
    const datosIdioma = {
      Certificado: Certificado,
      Identificacion: Identificacion,
      idioma: idioma,
      Nivel: Nivel
    };

    // ESTE POST HACE EL PREREGISTRO EN LA BASE DE DATOS
    this.http
      .post(URL_thebeersapp1 + '/RegistroIdiomas', datosIdioma)
      .subscribe(respuesta => {
        // console.log(respuesta);
        this.RTA = respuesta;
      });
  }

  CambioVisiblilidad(id_user: string, Visible: string): any {
    const datosVisible = {
      id_user: id_user,
      Visible: Visible
    };

    // ESTE POST HACE EL PREREGISTRO EN LA BASE DE DATOS
    this.http
      .post(URL_thebeersapp1 + '/CambioVisibilidad', datosVisible)
      .subscribe(respuesta => {
        // console.log(respuesta);
        this.RTA = respuesta;
      });
  }

  ActualizarCoordenadss(
    id_user: string,
    LatitudActual: string,
    LongitudActual: string
  ): any {
    const datosCoord = {
      id_user: id_user,
      LatitudActual: LatitudActual,
      LongitudActual: LongitudActual
    };

    // ESTE POST HACE EL PREREGISTRO EN LA BASE DE DATOS
    this.http
      .post(URL_thebeersapp1 + '/ActualizarCoordenadas', datosCoord)
      .subscribe(respuesta => {
        // console.log(respuesta);
        this.RTA = respuesta;
      });
  }

  RepartirTarjetas(
    latitud: string,
    longitud: string,
    Identificacion
  ): any {
    const datosCoord = {
      latitud: latitud,
      longitud: longitud,
      Identificacion: Identificacion
    };

    // ESTE POST HACE EL PREREGISTRO EN LA BASE DE DATOS
    this.http
      .post(URL_thebeersapp1 + '/RepartirTarjeta', datosCoord)
      .subscribe(respuesta => {
        // console.log(respuesta);
        this.Repartidas = respuesta;
      });
  }


  BuscarPersonas(
    latitud: string,
    longitud: string,
    Identificacion
  ): any {
    const datosCoord = {
      latitud: latitud,
      longitud: longitud,
      Identificacion: Identificacion
    };

    // ESTE POST HACE EL PREREGISTRO EN LA BASE DE DATOS
    this.http
      .post(URL_thebeersapp1 + '/PersonasCercanas', datosCoord)
      .subscribe(respuesta => {
        // console.log(respuesta);
        this.PersonasCercanas = respuesta;
      });
  }

  UltimoID(
    Identificacion: string,
    tipoPauta: string
  ): any {
    const datosID = {
      Identificacion: Identificacion,
      tipoPauta: tipoPauta
    };

    // ESTE POST HACE EL PREREGISTRO EN LA BASE DE DATOS
    this.http
      .post(URL_thebeersapp1 + '/ClasificadoID', datosID)
      .subscribe(respuesta => {
        // console.log(respuesta);
        this.IDRetornado = respuesta;
        console.log('ESTE ES EL ID QUERECUPERA DE LA BASE DE DATOS');
        console.log(this.IDRetornado);
      });
  }
  RegistrarClasificado(
    Identificacion: string,
    Titulo: string,
    Descripcion: string,
    Contacto: string,
    PalabrasClave: string,
    Tipo: string,
    Color: string,
    ID: string
  ): any {
    const datosClasf = {
      Identificacion: Identificacion,
      Titulo: Titulo,
      Descripcion: Descripcion,
      Contacto: Contacto,
      PalabrasClave: PalabrasClave,
      Tipo: Tipo,
      Color: Color,
      ID: ID
    };

    // ESTE POST HACE EL PREREGISTRO EN LA BASE DE DATOS-
    this.http
      .post(URL_thebeersapp1 + '/RegistrarClasificado', datosClasf)
      .subscribe(respuesta => {
        // console.log(respuesta);
        this.RTA_Clasificado = true;
      }, (error: any) => {
        // console.log(error);
        this.RTA_Clasificado = false;
      });
  }

  RegistrarFeriaEvento(
    Identificacion: string,
    Ciudad: string,
    Pais: string,
    Titulo: string,
    Direccion: string,
    fechaIni: string,
    fechaFin: string,
    precio: number,
    PalabrasClave: string,
    Tipo: string,
    Expositores,
    Color: string,
    ID: string
  ) {

    const datosFeriEvent = {
      Identificacion: Identificacion,
      Ciudad: Ciudad,
      Pais: Pais,
      Titulo: Titulo,
      Direccion: Direccion,
      fechaIni: fechaIni,
      fechaFin: fechaFin,
      precio: precio,
      PalabrasClave: PalabrasClave,
      Tipo: Tipo,
      Expositores: Expositores,
      Color: Color,
      ID: ID
    };

    // ESTE POST HACE EL PREREGISTRO EN LA BASE DE DATOS-
    this.http
      .post(URL_thebeersapp1 + '/RegistrarFeriaEvento', datosFeriEvent)
      .subscribe(respuesta => {
        console.log(respuesta);
        this.RTA_Feria_evento = true;
      }, (error: any) => {
        console.log(error);
        this.RTA_Feria_evento = false;
      });

  }

  comprobarContrasena(
    Identificacion: string,
    Contrasena: string
  ) {

    const datosContrasena = {
      Identificacion: Identificacion,
      Contrasena: Contrasena
    };

    // ESTE POST HACE EL PREREGISTRO EN LA BASE DE DATOS-
    this.http
      .post(URL_thebeersapp1 + '/ComprobarContrasena', datosContrasena)
      .subscribe((respuesta: any ) => {
        if (respuesta.mensaje == 'Incorrecta') {
          this.Contrasena = false;
        }
        if (respuesta.mensaje == 'Correcto') {

          this.Contrasena = true;
        }
        console.log(respuesta);
      }, (error: any) => {
        this.Contrasena = false;
        console.log(error);
      });

  }

  cargarEmpresaUsuarios(
    Empresa: string
  ) {

    const datosProfesion = {
      Empresa: Empresa
    };

    // ESTE POST HACE EL PREREGISTRO EN LA BASE DE DATOS-
    this.http
      .put(urls.URLphpCrud + 'usuarios/' + this.valor.identificacion, datosProfesion)
      .subscribe((respuesta: any) => {

        console.log(respuesta);
      }, (error: any) => {
        console.log(error);
      });

  }


  cargarPreciosTamanosPautas() {
    // if (this.data) {
    //  return of(this.data);
    // } else {
    return this.http
      .get(URL_thebeersapp1 + '/FeriasEventosAleatorios/PreciosTamanosPautas');
    // .pipe(map(this.processData, this));
    // }
  }

  VerAmigos(
    Identificacion: string
  ): any {
    const datosID = {
      Identificacion: Identificacion
    };

    // ESTE POST HACE EL PREREGISTRO EN LA BASE DE DATOS
    this.http
      .post(URL_thebeersapp1 + '/Amigos', datosID)
      .subscribe(respuesta => {
        // console.log(respuesta);
        this.Amigos = respuesta;
        this.CargaAmigosCompleta = true;
      }, (error: any) => {
        this.CargaAmigosCompleta = false;
      });
  }

  VerAmigosComun(
    Identificacion: string,
    ID_Publicador: string
  ): any {
    const datosID = {
      Identificacion: Identificacion,
      ID_Publicador: ID_Publicador
    };

    // ESTE POST HACE EL PREREGISTRO EN LA BASE DE DATOS
    this.http
      .post(URL_thebeersapp1 + '/AmigosEnComun', datosID)
      .subscribe(respuesta => {
        // console.log(respuesta);
        this.AmigosEnComun = respuesta;
        this.CargaAmigosComunCompleta = true;
      }, (error: any) => {
        this.CargaAmigosComunCompleta = false;
      });
  }

  ComprobarSolicitud(
    Identificacion: string,
    Id_Contacto: string
  ): any {
    const datosID = {
      Identificacion: Identificacion,
      Id_Contacto: Id_Contacto
    };

    // ESTE POST HACE EL PREREGISTRO EN LA BASE DE DATOS
    this.http
      .post(URL_thebeersapp1 + '/ComprobarSolicitud', datosID)
      .subscribe(respuesta => {
        // console.log(respuesta);
        this.SolicitudComprobada = respuesta;
      }, (error: any) => {
      });
  }

  Contactar(
    Identificacion: string,
    ID_Contacto: string
  ): any {
    const datosID = {
      Identificacion: Identificacion,
      ID_Contacto: ID_Contacto
    };

    // ESTE POST HACE EL PREREGISTRO EN LA BASE DE DATOS
    this.http
      .post(URL_thebeersapp1 + '/Contactar', datosID)
      .subscribe(respuesta => {
        this.ContactoCorrecto = true;
        // console.log(respuesta);
      }, (error: any) => {
        this.ContactoCorrecto = false;
      });
  }

  RevisarSolicitudes(
    Identificacion: string,
  ): any {
    const datosID = {
      Identificacion: Identificacion
    };

    // ESTE POST HACE EL PREREGISTRO EN LA BASE DE DATOS
    this.http
      .post(URL_thebeersapp1 + '/RevisarSolicitudes', datosID)
      .subscribe(respuesta => {
        this.solicitudesPendientes = respuesta;
        // // console.log(respuesta);
      }, (error: any) => {
         // console.log(error);
      });
  }

  ConfirmarSolicitud(
    Identificacion: string,
    Id_Contacto: string
  ): any {
    const datosID = {
      Identificacion: Identificacion,
      Id_Contacto: Id_Contacto
    };

    // ESTE POST HACE EL PREREGISTRO EN LA BASE DE DATOS
    this.http
      .post(URL_thebeersapp1 + '/ConfirmarSolicitud', datosID)
      .subscribe(respuesta => {
        // this.ContactoCorrecto = true;
        // console.log(respuesta);
        //// console.log(respuesta);
      }, (error: any) => {
        // this.ContactoCorrecto = false;
        // console.log(error);
      });
  }

  RechazarSolicitud(
    Identificacion: string,
    Id_Contacto: string
  ): any {
    const datosID = {
      Identificacion: Identificacion,
      Id_Contacto: Id_Contacto
    };

    // ESTE POST HACE EL PREREGISTRO EN LA BASE DE DATOS
    this.http
      .post(URL_thebeersapp1 + '/RechazarSolicitud', datosID)
      .subscribe(respuesta => {
        // this.ContactoCorrecto = true;
        // console.log(respuesta);
        //// console.log(respuesta);
      }, (error: any) => {
        // this.ContactoCorrecto = false;
        // console.log(error);
      });
  }

  GuardarContrasena(
    Contrasena: string,
    Identificacion: string
  ): any {
    const datosContrasena = {
      Contrasena: Contrasena,
      Identificacion: Identificacion
    };

    // ESTE POST HACE EL REGISTRO EN LA BASE DE DATOS
    this.http
      .post(URL_thebeersapp1 + '/Contrasena', datosContrasena)
      .subscribe(respuesta => {
        // console.log(respuesta);
        this.verificacionContrasena = true;
      }, (error: any) => {
        // console.log(error);
        this.verificacionContrasena = false;
      });
  }


  confirmarPIN(
    PINingresado: string,
    Identificacion: string
  ): any {
    const datosPIN = {
      PINingresado: PINingresado,
      Identificacion: Identificacion
    };

    // ESTE POST HACE EL REGISTRO EN LA BASE DE DATOS
    this.http
      .post(URL_thebeersapp1 + '/ConfirmarPin', datosPIN)
      .subscribe(respuesta => {
         console.log(respuesta);
        this.PINLlegado = respuesta;
      }, (error: any) => {
         console.log(error);
       // this.verificacionContrasena = false;
      });
  }

  generarPIN(
    Correo: string,
    Identificacion: string
  ): any {
    const datosPIN = {
      Correo: Correo,
      Identificacion: Identificacion
    };

    // ESTE POST HACE EL REGISTRO EN LA BASE DE DATOS
    this.http
      .post(URL_thebeersapp1 + '/GenerarPIN', datosPIN)
      .subscribe(respuesta => {
        console.log(respuesta);
        this.PINGenerado = respuesta;
      }, (error: any) => {
        console.log(error);
        // this.verificacionContrasena = false;
      });
  }

  /// Bandera para saber que se realizó el cambio de la contraseña
  contrasenaCambiada(
    Valor: string,
    Identificacion: string
  ): any {
    const datosCNT = {
      Valor: Valor,
      Identificacion: Identificacion
    };

    // ESTE POST HACE EL REGISTRO EN LA BASE DE DATOS
    this.http
      .post(URL_thebeersapp1 + '/ContrasenaCambiada', datosCNT)
      .subscribe(respuesta => {
        console.log(respuesta);
        // this.PINGenerado = respuesta;
      }, (error: any) => {
        console.log(error);
        // this.verificacionContrasena = false;
      });
  }
}
