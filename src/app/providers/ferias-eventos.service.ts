import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { of } from 'rxjs';

//import { UserData } from './user-data';
import * as urls from '../providers/urls.servicios';

@Injectable({
  providedIn: 'root'
})
export class FeriasEventosService {
  data: any;

  constructor(
    public http: HttpClient
  ) { }

  load(): any {
    if (this.data) {
      return of(this.data);
    } else {
      return this.http
        .get(urls.URL_thebeersapp1 + '/FeriasEventosAleatorios/FEAleatorios');
       // .pipe(map(this.processData, this));
    }
  }

  buscarFeriasEventosCiudad(cadenaFeriasEventosCiudad){
    let datosBusquedaFeriasEventosCiudad = {cadenaOriginal: cadenaFeriasEventosCiudad};
    // ESTE POST HACE REALIZA LA BUSQUEDA EN LA BASE DE DATOS
    return this.http
    .post(urls.URL_thebeersapp1 + "/FeriasEventosCiudad", datosBusquedaFeriasEventosCiudad);
  }

  loadPaises(): any {
    if (this.data) {
      return of(this.data);
    } else {
      return this.http
        .get(urls.URL_thebeersapp1 + '/FeriasEventosAleatorios/Paises');
       // .pipe(map(this.processData, this));
    }
  }

  loadCiudades(): any {
    if (this.data) {
      return of(this.data);
    } else {
      return this.http
        .get(urls.URL_thebeersapp1 + '/FeriasEventosAleatorios/Ciudades');
      // .pipe(map(this.processData, this));
    }
  }
}
