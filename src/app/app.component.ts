import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { SwUpdate } from '@angular/service-worker';

import { Events, MenuController, Platform, ToastController, LoadingController } from '@ionic/angular';

import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { Storage } from '@ionic/storage';

import { UserData } from './providers/user-data';
import { MapPage } from './pages/map/map';
import { Valores } from './providers/valores';
import * as urls from './providers/urls.servicios';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AppComponent implements OnInit {
  appPages = [
    {
      title: 'Principal',
      url: '/app/tabs/schedule',
      icon: 'home'
    },
    {
      title: 'Amigos',
      url: '/app/tabs/speakers',
      icon: 'beer'
    },
    {
      title: 'Promos',
      url: '/app/tabs/map',
      icon: 'star'
    },
    {
      title: 'Descubrir',
      url: '/app/tabs/about',
      icon: 'compass'
    }
  ];
  loggedIn = false;
  URLavatar: any;

  constructor(
    private events: Events,
    private menu: MenuController,
    private platform: Platform,
    private router: Router,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private storage: Storage,
    private userData: UserData,
    private swUpdate: SwUpdate,
    private toastCtrl: ToastController,
    public loadingController: LoadingController,
    public mapPage: MapPage,
    public valor: Valores
  ) {
    this.initializeApp();
    this.userData.getUserID();
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      /*  Environment.setEnv({
         // Api key for your server
         // (Make sure the api key should have Website restrictions for your website domain only)
         'API_KEY_FOR_BROWSER_RELEASE': 'AIzaSyB8pf6ZdFQj5qw7rc_HSGrhUwQKfIe9ICw',
 
         // Api key for local development
         // (Make sure the api key should have Website restrictions for 'http://localhost' only)
         'API_KEY_FOR_BROWSER_DEBUG': '(YOUR_API_KEY_IS_HERE)'
       }); */
      this.statusBar.overlaysWebView(false);
      this.statusBar.styleLightContent();
      this.statusBar.backgroundColorByHexString('#000000'); // define el color de la barra de notificaciones
    });
  }

  async ngOnInit() {

    this.userData.InicioSesion = false;
    this.userData.inicio();
    setTimeout(() => {
      this.checkLoginStatus();
    }, 2100);
   
   
    this.userData.cargarDatosActualizacion();
    this.URLavatar = urls.URLimagen; // URL de acceso a las imagenes
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  sesiones() {
    this.userData.inicio();
    setTimeout(() => {
      // this.evaluarvisibilidad();
    }, 700);
    setTimeout(() => {
      // this.evaluarvisibilidad();
    }, 700);
  }

  async presentLoading() {
    const loading = await this.loadingController.create({
      message: 'Iniciando sesión',
      duration: 1500
    });
    await loading.present();

  }

  logout() {
    // clearInterval(this.intervalo);
    this.userData.logout();
    this.menu.enable(false);
    this.valor.guardarid(null);
    this.valor.identificacion = null;
    console.log('Se cerró sesión');
    this.userData.datos = [];
    this.mapPage.finalizarRastreo();
    setTimeout(() => {
      this.sesiones();
    }, 300);
    setTimeout(() => {
      this.router.navigateByUrl('/welcome-page');
    }, 300);
    this.router.navigateByUrl('/welcome-page');
  }



  listenForLoginEvents() {
    this.events.subscribe('user:login', () => {
      this.updateLoggedInStatus(true);
    });

    this.events.subscribe('user:signup', () => {
      this.updateLoggedInStatus(true);
    });

    this.events.subscribe('user:logout', () => {
      this.updateLoggedInStatus(false);
    });
  }

  

  checkLoginStatus() {
    // console.log('cargo el checklogin: ' + this.valor.identificacion);
    if (this.valor.identificacion !== null) {
      // console.log('inicio sesion correctamente');
      this.updateLoggedInStatus(true);
    } if (this.valor.identificacion === null) {
      this.updateLoggedInStatus(false);
    }
  }

  updateLoggedInStatus(loggedIn: boolean) {
    setTimeout(() => {
      this.loggedIn = loggedIn;
      // console.log(this.loggedIn);
      if (this.loggedIn) {
        setTimeout(() => {
          this.sesiones();
          this.router.navigateByUrl('/app/tabs/schedule');
        }, 800);
        setTimeout(() => {
          this.userData.InicioSesion = true;
          this.mapPage.actualizarCoords();
        }, 3500);
        this.presentLoading();
        
      
      } else {
        this.valor.guardarid(null);
        this.userData.InicioSesion = false;
       // this.presentarToast();
        // console.log('Intervalo desactivado en LoggedIn');
      }
    }, 300);
  }

  getUserDataFromDB(iniciateSession: boolean) {
    if (iniciateSession === false) {
     // this.presentarToast();
    }
  }

  openTutorial() {
    this.menu.enable(false);
    this.storage.set('ion_did_tutorial', false);
    this.router.navigateByUrl('/tutorial');
  }
}
