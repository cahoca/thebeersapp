
export interface UserOptionsRegistration {
  name: string;
  surname: string;
  ID: string;
  celular: string;
  email: string;
  password: string;
  PIN?: String;
  genere?: string;
  direccion?: string;
  ciudad?: string;
}
