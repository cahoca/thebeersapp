export interface PostEmpresasOptions {
  RazonSocial: string;
  Direccion: string;
  Telefonos: string;
  Correos: string;
  Ciudad: string;
}
