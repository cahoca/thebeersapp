
export interface UserOptions {
  
  email: string;
  username: string;
  password: string;
  tel_cel: string;
  bornDate: string;
  id_user: string;
}
