
export interface DatosUsuarioInterface {
    error: boolean,
    mensaje: string,
    DatosPersonales: any[],
    Educacion: any[],
    Experiencia: any[]
}
