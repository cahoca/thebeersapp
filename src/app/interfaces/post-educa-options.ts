export interface PostEducaOptions {
  Nivel_Academico: string;
  Institucion_Educativa: string;
  Identificacion: string;
  Titulo_Obtenido: string;
  Ciudad: string;
  Fecha_Inicio: string;
  Fecha_Fin: string;
}
