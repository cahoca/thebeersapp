import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { IonicModule } from '@ionic/angular';
import { IonicStorageModule } from '@ionic/storage';
import { Geolocation } from '@ionic-native/geolocation/ngx';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';

// NUESTROS SERVICIOS
import { RegistroUsuariosService } from './providers/registro-usuarios.service';
import { Valores } from './providers/valores';
import { Ubicacion } from './providers/ubicacion';
import { MapPage } from './pages/map/map';
// import { FindPeoplePage } from './pages/find-people/find-people.page';
import { UserData } from './providers/user-data';
import { Camera } from '@ionic-native/camera/ngx';
import { FileTransfer } from '@ionic-native/file-transfer/ngx';

@NgModule({
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    IonicModule.forRoot(),
    IonicStorageModule.forRoot(),
    ServiceWorkerModule.register('ngsw-worker.js', {
      enabled: environment.production
    })
  ],
  declarations: [AppComponent],
  providers: [
    InAppBrowser,
    SplashScreen,
    StatusBar,
    RegistroUsuariosService,
    Ubicacion,
    Valores,
    MapPage,
    AppComponent,
    UserData,
    Geolocation,
    Camera,
    FileTransfer
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
